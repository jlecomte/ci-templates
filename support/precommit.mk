PRECOMMITFLAGS ?=

all: precommit

.PHONY: precommit pre-commit pc
precommit pre-commit pc: ## run pre-commit on all files
	pre-commit run -a $(PRECOMMITFLAGS)
