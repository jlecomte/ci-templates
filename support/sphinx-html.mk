SPHINXBUILDFLAGS ?=

.PHONY: html docs doc
html docs doc: ## build html documentation

.PHONY: sphinx sphinx-html
html docs: sphinx-html
sphinx: sphinx-html
sphinx-html: public
	if test -f docs/conf.py ; then \
	  PYTHONPATH=".:$$PYTHONPATH" sphinx-build -b html -t html $(SPHINXBUILDFLAGS) docs ./public/docs/html ; \
	fi
	@echo "Documentation generated here: file://$(shell pwd)/public/docs/html/index.html"

distclean: sphinx-html-distclean
sphinx-html-distclean:
	@rm -fr public/docs
