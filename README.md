# GitLab CI Templates

DEPRECATED since 2025-01-01. Please upgrade to https://gitlab.com/cappysan/ci-templates or fork this project.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
