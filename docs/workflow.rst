Workflow
========

The workflow CI file will be evaluated before jobs and is included by default in every CI.

Pipelines
---------

To prevent multiple pipelines the workflow is configured to run:

  * Branch pipelines when a merge request is not open for the branch.

  * Merge request pipelines when a merge request is open for the branch.

Allowing failures
-----------------

*Allow failure* for pipeline jobs will be enabled depending on if the error is considered to be fixable.

For example, *pylint* errors will be a pipeline job that allows failure on the *master* branch, but that doesn't on other branches.

.. rubric:: References

* `GitLab CI/CD workflow keyword <https://docs.gitlab.com/ee/ci/yaml/workflow.html>`__
