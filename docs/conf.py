# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import datetime
import importlib
import os

try:
    import tomlib
except ModuleNotFoundError:
    import tomli as tomlib

year = datetime.date.today().year

config = os.path.dirname(__file__) + "/../pyproject.toml"
with open(config, mode="rb") as fd:
    about = tomlib.load(fd)["project"]

# ------------------------------------------------------------------------------
# Project information
author = about["authors"][0]["name"]
copyright = f"{year}, {author}"
project = about["name"]
release = about["version"]

# ------------------------------------------------------------------------------
# General configuration
# ------------------------------------------------------------------------------
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
]

if importlib.util.find_spec("sphinx_autofixture") is not None:
    extensions += ["sphinx_autofixture"]

# cf: https://webknjaz.github.io/intersphinx-untangled/
intersphinx_mapping = {
    # "python": ("https://docs.python.org/3", None),
    # "flask": ("https://flask.palletsprojects.com/en/2.2.x/", None),
    # "attrs": ("https://www.attrs.org/en/stable/", None),
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

source_suffix = ".rst"

# When enabled, emphasize placeholders in command line option directives
option_emphasise_placeholders = True

autodoc_typehints_format = "short"
autodoc_preserve_defaults = False

smart_quotes = False

# ------------------------------------------------------------------------------
# Options for HTML output
# ------------------------------------------------------------------------------
try:
    html_theme = "sphinx_rtd_theme"
except:
    html_theme = "alabaster"

html_static_path = ["static"]
html_css_files = ["custom.css"]
html_show_sourcelink = False
html_use_smartypants = False

# ------------------------------------------------------------------------------
# Misc
# ------------------------------------------------------------------------------
# Roles:
#
#  ev:  Environment Variable
#  p:   Function parameter
#  url: URL
#
# cf: https://sphinx-rtd-theme.readthedocs.io/en/stable/demo/demo.html#inline-markup
# cf: docs/static/custom.css
rst_prolog = """
.. role:: ev
  :class: envvar

.. role:: p
  :class: param

.. role:: url
  :class: url
"""
rst_epilog = ""


def setup(app):
    pass
