Defaults
========

Sane defaults are set by the project. They can be overloaded by overwriting them in your :file:`.gitlab-ci.yml` file.

One of :p:`scripts` or :p:`trigger` keyword must present:

.. code-block:: yaml

  defaults:
    timeout: "1 day"
    script:
      - /bin/true

If overring :p:`before_script`, ensure to include a reference to the functions that create the collapsible sections and coloring, like so:

.. code-block:: yaml

  defaults:
    before_script:
      - !reference [".functions", before_script]

.. rubric:: References

* `GitLab CI/CD defaults keyword <https://docs.gitlab.com/ee/ci/yaml/#default>`__
