# Generic variables:
SPHINXBUILDFLAGS ?=

PRECOMMITFLAGS ?= --show-diff-on-failure

# ==============================================================================
# Generic
# ==============================================================================
.DEFAULT_GOAL:= all

.PHONY: all
all: docs

.PHONY: help
help: ## Show this help
	@echo "Available targets:"
	@grep '^[a-z].*:.*##' $(MAKEFILE_LIST) | sort | sed 's/\([a-z][a-z]*\)[^:]*:/\1:/' | awk -F ':.*?## ' 'NF==2 {printf "  %-26s%s\n", $$1, $$2}'
	@echo "Available variables:"
	@grep '^[A-Z].*=.*##' $(MAKEFILE_LIST) | sort | sed 's/\([A-Z][A-Z]*\)[^=]*=/\1=/' | awk -F '=.*?## ' 'NF==2 {printf "  %-26s%s\n", $$1, $$2}'

.PHONY: public
public:
	@mkdir -p public/reports public/pytest public/coverage

.PHONY: clean
clean: ## clean generated cache files

.PHONY: distclean
distclean: clean ## clean built files

.PHONY: html docs
html docs: ## build html documentation

.PHONY: precommit pre-commit pc
precommit pre-commit pc: ## run pre-commit on all files
	pre-commit run -a $(PRECOMMITFLAGS)

.PHONY: pyclean
clean: pyclean
pyclean:
	@rm -f *.pyc *.py,cover
	@-rm -fr __pycache__

.PHONY: pydistclean
distclean: pydistclean
pydistclean: pyclean
	@rm -fr public/docs public/coverage public/pytest public/reports
	@-rm -fr *.egg-info dist .pytest_cache .coverage
	@-if test -d public ; then rmdir public; fi

# ------------------------------------------------------------------------------
# sphinx documentation
.PHONY: sphinx pydoc pydoc-html
html docs: sphinx
sphinx pydoc pydoc-html: public
	if test -f docs/conf.py ; then \
	  PYTHONPATH=".:$$PYTHONPATH" sphinx-build -b html $(SPHINXBUILDFLAGS) docs ./public/docs/html ; \
	fi
	@echo "Documentation generated here: file://$(shell pwd)/public/docs/html/index.html"
